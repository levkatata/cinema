import styles from './App.module.css';
import Calendar from '../../images/calendar-btn.png';
import Screen from '../../images/screen.png';
import { Room } from '../room/room';
import { InfoBlock } from '../infoblock/infoblock';
import { createContext, useState } from 'react';

export const UserContext = createContext();

function App() {
  const [tickets, setTickets] = useState([]);
  const value = { tickets, setTickets };

  return (<UserContext.Provider value={value}>
    <div className={styles.divmain}>
      <div className={styles.content_wrap}>
        <header className={styles.header}>
          <div></div>
          <div>Оберіть місця</div>
          <img src={Calendar}/>
        </header>
        <img src={Screen} />
        <Room/>
        <div className={styles.legend}>
          <div>Вільно</div>
          <div>Зайнято</div>
          <div>Обрано</div>
        </div>
      </div>
      <footer className={styles.info_block}>
        <div className={styles.btn_buy}>Купити</div>
        <InfoBlock />
      </footer>
    </div>
    </UserContext.Provider>
  );
}

export default App;
