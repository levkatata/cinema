import { useContext } from 'react';
import { UserContext } from '../app/App';
import styles from './infoblock.module.css';

export function InfoBlock() {
    const context = useContext(UserContext);

    return (<div className={styles.divmain}>
        <div>Date time</div>
        <div>{'Назва * місце ' + context.tickets.join(", ")}</div>
        <div>{'Ціна ' + context.tickets.length * 100 + ' грн.'}</div>
    </div>);
}